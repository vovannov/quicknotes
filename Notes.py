#!/usr/bin/python3.5

import os
import tkinter as tk
import sys
from PyQt4 import QtCore, QtGui
from PIL import Image, ImageTk

# absolute path where the program and necessary image and txt file are located
ROOT_PATH = os.getcwd()


class SystemTrayIcon(QtGui.QSystemTrayIcon):
    def __init__(self, icon, parent=None):
        QtGui.QSystemTrayIcon.__init__(self, icon, parent)
        menu = QtGui.QMenu(parent)
        exit_action = QtGui.QAction("Exit", self)
        QtCore.QObject.connect(exit_action, QtCore.SIGNAL('triggered()'), sys.exit)
        settings = QtGui.QAction("Notes", self)  # menu item to call widget window
        QtCore.QObject.connect(settings, QtCore.SIGNAL('triggered()'), make_notes)
        menu.addAction(settings)
        menu.addAction(exit_action)
        self.setContextMenu(menu)


# main window where you can make notes
def make_notes():

    def save(content):
        content = content.strip()
        with open(os.path.join(ROOT_PATH, 'notes.txt'), 'w') as file:
            file.write(content)

    def on_closing():
        content = textbox.get(1.0, tk.END)
        save(content)
        root.destroy()

    def clear():
        textbox.delete(1.0, tk.END)

    def info():
        info_window = tk.Toplevel()
        info_window.title('About')
        info_window.geometry('400x400+600+300')
        info_window.configure(background='white')
        info_label = tk.Message(info_window, font='Verdana 16 bold', width=380, text='This is simple program where '
                                'you can easily make some notes', background='white')
        shortcut_label = tk.Message(info_window, font='Verdana 16', width=380, text='In order to make notes easier, '
                                    'there are some keyboard shortcuts for text window:\n'
                                    '➤ ctrl-a - select all text\n'
                                    '➤ ctrl-s - save all text in "notes.txt" file\n'
                                    '➤ ctrl-j - make "arrow" symbol\n'
                                    '➤ ctrl-backspace - delete word under the cursor\n'
                                    '➤ ctrl-delete - delete whole line\n',
                                    background='white')
        info_label.pack()
        shortcut_label.pack()
        info_window.mainloop()

    root = tk.Tk()
    root.geometry('400x300+1000+0')
    root.minsize(100, 100)
    root.title('Quicknotes')
    root.iconphoto(True, tk.PhotoImage(file=(os.path.join(ROOT_PATH, 'images/notepad.png'))))
    root.lift()
    root.attributes('-topmost', True)

    root.bind_class("Text", "<Control-a>", lambda event: textbox.tag_add(tk.SEL, 1.0, tk.END))
    root.bind_class("Text", "<Control-s>", lambda event: save(textbox.get(1.0, tk.END)))
    root.bind_class("Text", "<Control-j>", lambda event: textbox.insert(tk.INSERT, '➤'))
    root.bind_class("Text", "<Control-BackSpace>", lambda event: textbox.delete('insert wordstart', 'insert wordend'))
    root.bind_class("Text", "<Control-Delete>", lambda event: textbox.delete('insert linestart', 'insert lineend'))

    textbox = tk.Text(root, font='Verdana 16')
    with open(os.path.join(ROOT_PATH, 'notes.txt'), 'r') as f:
        textbox.insert(1.0, f.read())

    toolbar = tk.Frame(root, bd=1)

    save_img = Image.open(os.path.join(ROOT_PATH, "images/floppy_disc.png"))
    tk_save_img = ImageTk.PhotoImage(save_img)
    save_button = tk.Button(toolbar, image=tk_save_img, relief=tk.FLAT, command=lambda: save(textbox.get(1.0, tk.END)))
    save_button.pack(side=tk.LEFT, anchor=tk.W, padx=2, pady=2)

    erase_img = Image.open(os.path.join(ROOT_PATH, "images/erase.png"))
    tk_erase_img = ImageTk.PhotoImage(erase_img)
    erase_button = tk.Button(toolbar, image=tk_erase_img, relief=tk.FLAT, command=clear)
    erase_button.pack(side=tk.LEFT, anchor=tk.W, padx=2, pady=2)

    info_img = Image.open(os.path.join(ROOT_PATH, "images/get_info.png"))
    tk_info_img = ImageTk.PhotoImage(info_img)
    info_button = tk.Button(toolbar, image=tk_info_img, relief=tk.FLAT, command=info)
    info_button.pack(side=tk.LEFT, anchor=tk.W, padx=2, pady=2)

    exit_img = Image.open(os.path.join(ROOT_PATH, "images/exit.png"))
    tk_exit_img = ImageTk.PhotoImage(exit_img)
    exit_button = tk.Button(toolbar, image=tk_exit_img, relief=tk.FLAT, command=on_closing)
    exit_button.pack(side=tk.LEFT, anchor=tk.W, padx=2, pady=2)

    toolbar.pack(side=tk.TOP, fill=tk.X)

    textbox.pack(fill=tk.BOTH, expand=tk.TRUE)

    root.protocol("WM_DELETE_WINDOW", on_closing)
    root.mainloop()


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    w = QtGui.QWidget()
    tray_icon = SystemTrayIcon(QtGui.QIcon(os.path.join(ROOT_PATH, 'images/notepad.png')), w)
    tray_icon.show()
    app.exec_()
