# quicknotes
A simple quicknotes widget written in Python 3.

This widget is written using Python 3 and tkinter library, it can work on both Windows and Linux-based operation systems. Widget is really simple and easy to use. It even has a system tray icon.

Custom shortcuts:
    ctrl-j - insert arrow symbol,
    ctrl-a - select all text in text widget(does not work in tkinter by default),
    ctrl-s - save all text in notes.txt,
    ctrl-backspace - delete word under the cursor,

###Screenshots: 
Second icon is the application icon in system tray (on Ubuntu 16.04)

![2016-07-08 12-39-22](https://cloud.githubusercontent.com/assets/19861595/16683888/b3a94210-450a-11e6-9b57-1d05d6f66836.png)

![2016-07-08 12-49-35](https://cloud.githubusercontent.com/assets/19861595/16683889/b51dc008-450a-11e6-91ca-dd1fadf887ac.png)



